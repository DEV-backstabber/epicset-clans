package me.backstabber.epicsetclans.clanhandles.saving;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.inject.Inject;

import me.backstabber.epicsetclans.EpicSetClans;
import me.backstabber.epicsetclans.clanhandles.data.EpicClanData;
import me.backstabber.epicsetclans.clanhandles.manager.YMLManager;
import me.backstabber.epicsetclans.enums.ClanNodes;
import me.backstabber.epicsetclans.utils.CommonUtils;

public class LocalSaving implements Saving {
	@Inject
	private EpicSetClans plugin;
	private HashMap<String, EpicClanData> clans=new HashMap<String, EpicClanData>();
	@Override
	public void setup() {
		if(!plugin.getSettings().getBoolean("mysql.enabled"))
		{
			reloadAllClans();
			int interval=(int) CommonUtils.evaluateString(plugin.getSettings().getFile().getString("settings.clan-backup-interval"));
			new BukkitRunnable() 
			{
				@Override
				public void run() 
				{
					Bukkit.getConsoleSender().sendMessage(CommonUtils.chat("[EpicSet-Clans] &aBacking up clans data."));
					Bukkit.broadcast(CommonUtils.chat("&b&lClans &7&l>> &f Backing up clans data."), "epicset.clans.op");
					backup();
				}
			}.runTaskTimer(plugin, interval, interval);
		}
	}

	@Override
	public void reloadAllClans() {
		for(String filename:plugin.getAllFiles("/clans"))
		{
			if(!filename.equals("sample"))
			{
				//load from normal clan location
				if(CommonUtils.checkConfig(new File(plugin.getDataFolder()+"/clans",filename+".yml")))
				{
					YMLManager file=new YMLManager(plugin, filename, "/clans");
					EpicClanData data=new EpicClanData(file.getFile());
					plugin.injectMembers(data);
					data.setup();
					clans.put(filename, data);
					file.save(new File(plugin.getDataFolder()+"/clanbackups",filename+".yml"),true);
					continue;
				}
				Bukkit.getConsoleSender().sendMessage(CommonUtils.chat("[EpicSet-Clans] &aCould'nt load clan data for &c"+filename+" &a's clan. Searching backups."));
				//if normal is corrupted & a backup existed
				if(CommonUtils.checkConfig(new File(plugin.getDataFolder()+"/clanbackups",filename+".yml")))
				{
					Bukkit.getConsoleSender().sendMessage(CommonUtils.chat("[EpicSet-Clans] &aFound backup for &c"+filename+" &a's clan. Using that instead."));
					YMLManager file=new YMLManager(plugin, filename, "/clanbackups");
					EpicClanData data=new EpicClanData(file.getFile());
					plugin.injectMembers(data);
					data.setup();
					clans.put(filename, data);
					file.save(new File(plugin.getDataFolder()+"/clans",filename+".yml"),true);
					continue;
				}
				Bukkit.getConsoleSender().sendMessage(CommonUtils.chat("[EpicSet-Clans] &cCould'nt find any backup for &c"+filename+" &c's clan. His clan will be reset."));
				//if no backup could be found
				new File(plugin.getDataFolder()+"/clans",filename+".yml").delete();
				new File(plugin.getDataFolder()+"/clanbackups",filename+".yml").delete();
			}
		}
	}
	@Override
	public void updateClan(String player) {
		
	}
	@Override
	public EpicClanData addNewClan(String leader, String clanName) {
		//create file
		EpicClanData data=new EpicClanData();
		plugin.injectMembers(data);
		data.setupNew(leader, clanName);
		clans.put(leader, data);
		//save locally
		File main=new File(plugin.getDataFolder()+"/clans",leader+".yml");
		try {
			data.getFile().save(main);
		} catch (IOException e) {
		}
		return data;
	}
	@Override
	public void deleteClan(String leader) {
		//delete from memory
		clans.remove(leader);
		//delete locally
		YMLManager file=new YMLManager(plugin, leader,"/clans");
		file.delete();
		
	}
	@Override
	public void deleteClan(FileConfiguration file) {
		if(!file.isSet(ClanNodes.CLAN_LEADER.node()))
			return;
		deleteClan(file.getString(ClanNodes.CLAN_LEADER.node()));
	}
	@Override
	public EpicClanData getClan(String member) {
		for(EpicClanData data:clans.values())
			if(data.getClanMembersName().contains(member))
				return data;
			else 
			{
				String name=CommonUtils.chat(member);
				name=ChatColor.stripColor(name);
				if(data.getClanNameRaw().equalsIgnoreCase(name))
					return data;
			}
		return null;
	}
	@Override
	public void saveClan(FileConfiguration file) {
		//save in memory
		EpicClanData data=new EpicClanData(file);
		plugin.injectMembers(data);
		data.setup();
		clans.put(data.getClanLeader(), data);
		//save locally
		File fileLocation=new File(plugin.getDataFolder()+"/clans",data.getClanLeader()+".yml");
		if(!fileLocation.exists())
			try {
				fileLocation.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		try {
			file.save(fileLocation);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Override
	public boolean isInClan(String player) {
		//check in memory
		for(EpicClanData data:clans.values())
			if(data.getClanMembersName().contains(player))
				return true;
		return false;
	}
	@Override
	public boolean isLeader(String player) {
		//check in memory
		for(EpicClanData data:clans.values())
			if(data.getClanLeader().equals(player))
				return true;
		return false;
	}
	@Override
	public boolean isClanName(String name) {
		name=CommonUtils.chat(name);
		name=ChatColor.stripColor(name);
		//check in memory
		for(EpicClanData data:clans.values())
			if(data.getClanNameRaw().equals(name))
				return true;
		return false;
	}
	
	
	private void backup() 
	{
		ArrayList<EpicClanData> files = new ArrayList<>();
		for(EpicClanData data:clans.values())
			files.add(data);
		new BukkitRunnable() 
		{
			int t=files.size();
			@Override
			public void run() 
			{
				if(t<=0)
				{

					Bukkit.getConsoleSender().sendMessage(CommonUtils.chat("[EpicSet-Clans] &aBacking up completed."));
					Bukkit.broadcast(CommonUtils.chat("&b&lClans &7&l>> &f Backing up completed."), "epicset.clans.op");
					this.cancel();
				}
				else
				{
					FileConfiguration file=files.get(t-1).getFile();
					File main=new File(plugin.getDataFolder()+"/clans",files.get(t-1).getClanLeader()+".yml");
					File backup=new File(plugin.getDataFolder()+"/clanbackups",files.get(t-1).getClanLeader()+".yml");
					try {
						file.save(main);
						file.save(backup);
					} catch (IOException e) {
					}
					t--;
				}
			}
		}.runTaskTimer(plugin, 10, 20);
	}

	@Override
	public void saveClanFast(FileConfiguration file) {
		String leader=file.getString(ClanNodes.CLAN_LEADER.node());
		//save locally
		File fileLocation=new File(plugin.getDataFolder()+"/clans",leader+".yml");
		if(!fileLocation.exists())
			try {
				fileLocation.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		try {
			file.save(fileLocation);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Collection<EpicClanData> getAllClans() {
		return clans.values();
	}
}
