package me.backstabber.epicsetclans.guis;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import me.backstabber.epicsetclans.EpicSetClans;
import me.backstabber.epicsetclans.clanhandles.data.EpicClanData;
import me.backstabber.epicsetclans.clanhandles.data.ClanPlayersData;
import me.backstabber.epicsetclans.clanhandles.manager.ClanDuelManager;
import me.backstabber.epicsetclans.clanhandles.manager.ClanTopManager;
import me.backstabber.epicsetclans.clanhandles.manager.EpicClanManager;
import me.backstabber.epicsetclans.utils.CommonUtils;
import me.backstabber.epicsetclans.utils.materials.EpicMaterials;
import me.backstabber.epicsetclans.utils.materials.UMaterials;

public class ClanTop extends Guiable
{
	public ClanTop(EpicSetClans plugin,EpicClanManager clanManager,ClanDuelManager duelManager,ClanTopManager topManager)
	{
		super(plugin, clanManager, duelManager, topManager);
		guiName="top";
		file=plugin.getFiles().get(guiName);
	}
	@Override
	public Inventory getMainInventory(EpicClanData clan,Player player) 
	{
		Inventory inv=Bukkit.createInventory(null, 9*file.getInt("maingui.size"),CommonUtils.chat(file.getString("maingui.name")));
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if(file.isSet("maingui.background"))
				{
					ItemStack background = CommonUtils.getItemFromNode(file.getFile().getConfigurationSection("maingui.background"));
					background=CommonUtils.setStringTag(background, "gui:"+guiName+":background");
					for(int i=0;i<9*file.getInt("maingui.size");i++)
						inv.setItem(i, background);
				}
				if(file.isSet("maingui.data"))
				{
					String iname=file.getString("maingui.data.name");
					String itype=file.getString("maingui.data.type");
					List<String> ilore=file.getStringList("maingui.data.lore");
					int slot=file.getInt("maingui.data.slot");
					ItemStack clanData=CommonUtils.getCustomItem(itype, iname, ilore);
					clanData=CommonUtils.setStringTag(clanData, "gui:"+guiName+":clandata");
					inv.setItem(slot, clanData);
				}

				if(file.isSet("maingui.clandata"))
				{
					for(int slot=10;slot<=16;slot++)
					{
						if(topManager.getClan(slot-9)!=null)
						{
							EpicClanData data=(EpicClanData) topManager.getClan(slot-9);
							String iname=data.replacePlaceholders(file.getString("maingui.clandata.name")).replace("%pos%", String.valueOf(slot-9));
							String itype=data.replacePlaceholders(file.getString("maingui.clandata.type")).replace("%pos%", String.valueOf(slot-9));
							List<String> ilore=new ArrayList<String>();
							for(String s:data.replacePlaceholders(file.getStringList("maingui.clandata.lore")))
								ilore.add(s.replace("%pos%", String.valueOf(slot-9)));
							ItemStack playerData=CommonUtils.getCustomItem(itype, iname, ilore);
							playerData=plugin.getMonthlyRewardManager().addLore(playerData, slot-9);
							playerData=CommonUtils.setStringTag(playerData, "gui:"+guiName+":clandata:"+data.getClanLeader());
							inv.setItem(slot, playerData);
						}
					}
				}
				player.updateInventory();
			}
		}.runTaskAsynchronously(plugin);
		return inv;
	}

	@Override
	public Inventory getSubInventory(EpicClanData clan,ClanPlayersData data,Player player) 
	{
		return null;
	}

	@Override
	public void inventoryClickHandle(InventoryClickEvent event) 
	{
		//test to see if the clicked item was part of the gui (Unique way without using inventory name) 
		if(event.getRawSlot()<0)
			return;
		ItemStack item=event.getCurrentItem();
		if(item==null||item.getType().equals(EpicMaterials.valueOf(UMaterials.AIR).getMaterial()))
			return;
		String tag=CommonUtils.getStringTag(item);
		if(tag==null||tag.equals(""))
			return;
		if(!tag.startsWith("gui:"+guiName))
			return;
		event.setCancelled(true);
		if(tag.split(":").length==4&&tag.split(":")[2].equals("clandata"))
		{
			String clanName=tag.split(":")[3];
			if(clanManager.getClanData(clanName)==null)
				return;
			EpicClanData clan=(EpicClanData) clanManager.getClanData(clanName);
			Inventory stats = plugin.getGuis().get("stats").getMainInventory(clan,(Player) event.getWhoClicked());
			event.getWhoClicked().openInventory(stats);
		}
	}
	@Override
	public void inventoryCloseHandle(InventoryCloseEvent event) 
	{
		
	}
}
